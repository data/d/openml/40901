# OpenML dataset: Shuttle_7percent

https://www.openml.org/d/40901

**WARNING: This dataset is still in preparation.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Modified &ldquo;Statlog (Shuttle)&rdquo; dataset from the UCI machine learning for Unsupervised Anomaly Detection. The Statlog (Shuttle) data which is available at UCI repository. In our ex- periments, instances from class 1 are considered as normal points and instances from class 2, 3, 5, 6, 7 are anomalies. The anomalies ratios is 7.15%. Instances from the other classes are omitted. This dataset is not the original dataset. The target variable &quot;Target&quot; is relabeled into &quot;Normal&quot; and &quot;Anomaly&quot;.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/40901) of an [OpenML dataset](https://www.openml.org/d/40901). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/40901/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/40901/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/40901/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

